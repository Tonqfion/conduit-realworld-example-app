echo "=========== Setting up environment =========== "
ssh vagrant@192.168.56.10 "touch /var/www/express/.env"
ssh vagrant@192.168.56.10 "echo "DB_USER=$DB_USER" > /var/www/express/.env"
ssh vagrant@192.168.56.10 "echo "DB_DATABASE=$DB_DATABASE" >> /var/www/express/.env"
ssh vagrant@192.168.56.10 "echo "DB_PASSWORD=$DB_PASSWORD" >> /var/www/express/.env"
ssh vagrant@192.168.56.10 "echo "PORT=$PORT" >> /var/www/express/.env"
ssh vagrant@192.168.56.10 "echo "JWT_KEY=$JWT_KEY" >> /var/www/express/.env"
echo "=========== Environment set up successfully =========== "

