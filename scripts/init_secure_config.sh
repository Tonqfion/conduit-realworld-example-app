echo "=========== Setting up security configuration =========== "
command -v ssh-agent >/dev/null || ( apt-get update -y && apt-get install openssh-client -y )
eval $(ssh-agent -s)
echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
echo "=========== Security configuration set up successfully =========== "
